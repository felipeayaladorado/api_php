/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100416
 Source Host           : localhost:3306
 Source Schema         : updsdemo

 Target Server Type    : MySQL
 Target Server Version : 100416
 File Encoding         : 65001

 Date: 25/02/2021 21:43:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for producto
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `precio` float(10, 0) NULL DEFAULT NULL,
  `cantidad` float(10, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of producto
-- ----------------------------
INSERT INTO `producto` VALUES (1, 'Parlante 30w', 150, 20);
INSERT INTO `producto` VALUES (2, 'Auriculares', 210, 30);
INSERT INTO `producto` VALUES (3, 'Teclado', 70, 50);
INSERT INTO `producto` VALUES (4, 'Ratón', 240, 25);

SET FOREIGN_KEY_CHECKS = 1;
